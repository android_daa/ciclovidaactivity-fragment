package cl.duoc.ciclodevida;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LC1300XXXX on 04/10/2017.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {

    List<Fragment> fragments;
    List<String> fragmentTitle;

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
        fragments = new ArrayList<>();
        fragmentTitle = new ArrayList<>();
    }

    public void agregarFragment(Fragment fragment, String title){
        fragments.add(fragment);
        fragmentTitle.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
